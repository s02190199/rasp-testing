Interceptor.attach(Module.getExportByName(null, 'fopen'), {
  onEnter(args) {
      this.strstr = null;
      if (args[0].readUtf8String() == "/proc/self/maps")
      {
          this.strstr = Interceptor.attach(Module.getExportByName(null, 'strstr'), {
              onEnter(args) {
                  this.arg_1 = args[0].readUtf8String();
              },
              onLeave(retval) {
                  if (this.arg_1.includes('frida'))
                  {
                      retval.replace(0);
                  }
              }
          });
      }
  },
  onLeave(retval) {
      if (this.strstr)
      {
          this.strstr.detach();
      }
  }
});
    var currentBufferAddress = null;
    var myExportAddr1 = Module.findExportByName(null, "fgets")
    if (myExportAddr1 != null) {
      Interceptor.attach(myExportAddr1, {
        onEnter: function(args) {
            currentBufferAddress = args[0]          
      },
      onLeave: function(retval){
        var current_str = Thread.backtrace(this.context, Backtracer.FUZZY)
        .map(DebugSymbol.fromAddress).join('\n');
        if (current_str.indexOf("libodihifaklfgn.so") >= -1) {
        console.log('Called from:\n' +
        current_str + '\n');
        }
          console.log("Return value: " + ptr(retval).readUtf8String())
          console.log("Replace w with - in resulted string...")
          var what_we_got = ptr(retval).readUtf8String().replace("rw-p", "r--p")
          currentBufferAddress.writeUtf8String(what_we_got)
          ptr(retval).writeUtf8String(what_we_got)
          console.log("Resulted return value: " + ptr(retval).readUtf8String())
      }})
    }